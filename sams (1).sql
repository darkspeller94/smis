-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2020 at 07:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sams`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts_tbl`
--

CREATE TABLE `accounts_tbl` (
  `acc_no` bigint(20) NOT NULL,
  `id_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts_tbl`
--

INSERT INTO `accounts_tbl` (`acc_no`, `id_number`) VALUES
(1, 't17-073');

-- --------------------------------------------------------

--
-- Table structure for table `account_tbl`
--

CREATE TABLE `account_tbl` (
  `acc_no` bigint(20) NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('ADMIN','OFFICER','STUDENT') NOT NULL,
  `id_number` int(20) NOT NULL,
  `security_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account_tbl`
--

INSERT INTO `account_tbl` (`acc_no`, `user`, `password`, `role`, `id_number`, `security_code`) VALUES
(1, '0012685706', 'neustadmin', 'ADMIN', 0, 'varchar');

-- --------------------------------------------------------

--
-- Table structure for table `activity_tbl`
--

CREATE TABLE `activity_tbl` (
  `activity_number` bigint(20) NOT NULL,
  `activity_name` varchar(30) NOT NULL,
  `description` varchar(50) NOT NULL,
  `date_of_activity` date NOT NULL,
  `end_date` date NOT NULL,
  `Time_start` varchar(7) NOT NULL,
  `Time_end` varchar(7) NOT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_tbl`
--

INSERT INTO `activity_tbl` (`activity_number`, `activity_name`, `description`, `date_of_activity`, `end_date`, `Time_start`, `Time_end`, `time`) VALUES
(1, 'PTA MEETING', 'MEETING', '2020-08-09', '2020-08-09', '9:20PM', '12:30AM', '15:10');

-- --------------------------------------------------------

--
-- Table structure for table `guardian_tbl`
--

CREATE TABLE `guardian_tbl` (
  `id_number` varchar(20) NOT NULL,
  `guardian_name` varchar(50) NOT NULL,
  `guardian_number` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guardian_tbl`
--

INSERT INTO `guardian_tbl` (`id_number`, `guardian_name`, `guardian_number`) VALUES
('', '', ''),
('T17-063', 'JOSEPHINE NAZAR', '09424850291'),
('T17-073', 'RIA MUDLONG', '09103919683'),
('T17-001', '', '09120002122'),
('T17-002', '', '09191099999'),
('T17-003', '', '09294208421'),
('T18-001', '', '09900011223'),
('T18-002', '', '0929102343'),
('T18-003', '', ''),
('T18-004', '', ''),
('T18-005', '', '09910000119'),
('T19-001', '', '09355943221'),
('T19-002', '', '0909908424'),
('T19-003', 'RIA MUDLONG', '09103919683'),
('T19-004', '', '09103544551'),
('T19-005', '', '09223382222'),
('T20-001', '', ''),
('T20-002', 'JOSIE NAZAR', '09424850291'),
('T20-003', '', ''),
('T20-004', 'NENITA VILLANUEVA', '09338267786'),
('T20-005', '', '09985600768'),
('T17-007', '', ''),
('T13-131', '0000', ''),
('222', '', ''),
('222', '', ''),
('133', 'k', 'n'),
('323', 'kn', ''),
('22', 'jj', 'h'),
('3232', 'testing', '09261234567'),
('32423', 'sfsfsd', '4242'),
('432', 'ads', '331'),
('34', 'sdf', '324'),
('34', 'sdf', '4534'),
('4', 'asd', '43345'),
('4', 'gjg', '78686'),
('6', 'gjhgj', '78686'),
('6', 'hkjhk', '78'),
('6', 'kjhh', '879'),
('4', 'kh', '78'),
('6', 'kjhh', '879'),
('0001', 'LANIE PANGILINAN', '12345'),
('888', 'hkh', '987');

-- --------------------------------------------------------

--
-- Table structure for table `officer_tbl`
--

CREATE TABLE `officer_tbl` (
  `id_number` varchar(20) NOT NULL,
  `position` enum('ADMIN','OFFICER','STUDENT') NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `officer_tbl`
--

INSERT INTO `officer_tbl` (`id_number`, `position`, `date_created`) VALUES
('T17-073', 'ADMIN', '2020-11-09');

-- --------------------------------------------------------

--
-- Table structure for table `penalty_tbl`
--

CREATE TABLE `penalty_tbl` (
  `penalty_id` bigint(20) NOT NULL,
  `activity_number` bigint(20) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `status` set('Absent','Present','Unfinished') NOT NULL,
  `remarks` enum('Unsettled','Settled') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penalty_tbl`
--

INSERT INTO `penalty_tbl` (`penalty_id`, `activity_number`, `id_number`, `status`, `remarks`) VALUES
(1, 1, 'T17-001', 'Absent', 'Unsettled'),
(2, 1, 'T17-002', 'Absent', 'Unsettled'),
(3, 1, 'T17-003', 'Absent', 'Unsettled'),
(4, 1, 'T17-007', 'Absent', 'Unsettled'),
(5, 1, 'T17-063', 'Absent', 'Unsettled'),
(6, 1, 'T17-073', 'Absent', 'Unsettled'),
(7, 1, 'T18-001', 'Absent', 'Unsettled'),
(8, 1, 'T18-002', 'Absent', 'Unsettled'),
(9, 1, 'T18-003', 'Absent', 'Unsettled'),
(10, 1, 'T18-004', 'Absent', 'Unsettled'),
(11, 1, 'T18-005', 'Absent', 'Unsettled'),
(12, 1, 'T19-001', 'Absent', 'Unsettled'),
(13, 1, 'T19-002', 'Absent', 'Unsettled'),
(14, 1, 'T19-004', 'Absent', 'Unsettled'),
(15, 1, 'T19-005', 'Absent', 'Unsettled'),
(16, 1, 'T20-001', 'Absent', 'Unsettled'),
(17, 1, 'T20-002', 'Absent', 'Unsettled'),
(18, 1, 'T20-003', 'Absent', 'Unsettled'),
(19, 1, 'T20-004', 'Absent', 'Unsettled'),
(20, 1, 'T20-005', 'Absent', 'Unsettled');

-- --------------------------------------------------------

--
-- Table structure for table `studentlog_tbl`
--

CREATE TABLE `studentlog_tbl` (
  `log_id` int(11) NOT NULL,
  `id_number` varchar(11) NOT NULL,
  `remarks` varchar(11) NOT NULL,
  `time` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `students_tbl`
--

CREATE TABLE `students_tbl` (
  `id_number` varchar(20) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `year_level` varchar(3) NOT NULL,
  `section` varchar(2) NOT NULL,
  `program` varchar(50) NOT NULL,
  `contact_number` varchar(13) NOT NULL,
  `address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_tbl`
--

INSERT INTO `students_tbl` (`id_number`, `last_name`, `first_name`, `middle_name`, `year_level`, `section`, `program`, `contact_number`, `address`) VALUES
('T17-001', 'SANDAN', 'DINALYN', '', '4th', 'B', 'BSIT', '09616082910', 'BALUGA, TALAVERA ,NUEVA ECIJA'),
('T17-002', 'DELA CRUZ', 'ISRAEL', 'SALAZAR', '4th', 'B', 'BSIT', '09129090901', 'LA TORRE, TALAVERA, NUEVA ECIJA'),
('T17-003', 'CHUA', 'RAPHAEL', 'DELA CRUZ', '4th', 'A', 'BSIT', '09112020200', 'CABANATUAN,NUEVA ECIJA'),
('T17-007', 'MUDLONG', 'MARY ROSE', 'CELA CRUZ', '4th', 'A', 'BSIT', '09412200111', 'PAG-ASA TALAVERA NUEVA ECIJA'),
('T17-063', 'NAZAR', 'MELISSA JOY', 'VILLAFUERTE', '4th', 'B', 'BSIT', '09616082910', 'PAG-ASA, TALAVERA , NUEVA ECIJA'),
('T17-073', 'MUDLONG', 'RAMONCITO', 'DELA CRUZ', '4th', 'B', 'BSIT', '09073553402', 'PALUDPOD, TALAVERA, NUEVA ECIJA'),
('T18-001', 'BULANADI', 'JOHN MIGUEL', 'CASIMIRO', '3rd', 'A', 'BSIT', '09071821818', 'ESGUERA DIST. TALAVERA, NUEVA ECIJA'),
('T18-002', 'BAUTISTA', 'VANESSA', '', '3rd', 'A', 'BSIT', '09129029022', 'SAN PASCUAL, TALAVERA, NUEVA ECIJA'),
('T18-003', 'PANGILINAN', 'ADRIAN', 'FERNANDEZ', '3rd', 'C', 'BSIT', '09224406001', 'ESGUERA DIST., TALAVERA, NUEVA ECIJA'),
('T18-004', 'REALINA', 'JOVETH', 'TANAGRAS', '3rd', 'C', 'BSIT', '09099013991', 'SAN RICARDO, TALAVERA, NUEVA ECIJA'),
('T18-005', 'LOPEZ', 'VINCENT JOHN', 'ALABADO', '3rd', 'A', 'BSIT', '09190012367', 'MATIAS, TALAVERA, NUEVA ECIJA'),
('T19-001', 'RODRIGUES', 'ALEXANDER', 'VILLAFUERTE', '2nd', 'D', 'BSIT', '09173838445', 'PAG-ASA, TALAVERA, NUEVA ECIJA'),
('T19-002', 'ADION', 'LALAINE', 'VILLAFUERTE', '2nd', 'A', 'BSIT', '09150099222', 'PAG-ASA, TALAVERA, NUEVA ECIJA'),
('T19-004', 'TALAVERA', 'MARK VINCENT', 'GERNALINE', '2nd', 'C', 'BSIT', '09156890435', 'LOMBOY, TALAVERA, NUEVA ECIJA'),
('T19-005', 'WY', 'RONALD', 'TIANGCO', '2nd', 'B', 'BST', '09150966699', 'PINAGPANAAN, TALAVERA, NUEVA ECIJA'),
('T20-001', 'CONSTANTINO', 'ROMAN', 'DELA CRUZ', '1st', 'D', 'BSIT', '09616082910', 'BALUGA, TALAVERA, NUEVA ECIJA'),
('T20-002', 'SERAFIN', 'SAMANTHA', 'MARGUAX', '1st', 'A', 'BSIT', '09150099445', 'PAG-ASA, TALAVERA, NUEVA ECIJA'),
('T20-003', 'LAYUG', 'JESSICA', 'SERBANTES', '1st', 'A', 'BSIT', '09223955356', 'PULA,TALAVERA, NUEVA ECIJA'),
('T20-004', 'VILLANUEVA', 'JOEY', 'VILLAFUERTE', '1st', 'A', 'BSIT', '09110091299', 'POBLACION SUR, TALAVERA, NUEVA ECIJA'),
('T20-005', 'PONCE', 'JOMEL', 'MARTINES', '1st', 'D', 'BSIT', '09473753333', 'SICSICAN, TALAVERA, NUEVA ECIJA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts_tbl`
--
ALTER TABLE `accounts_tbl`
  ADD PRIMARY KEY (`acc_no`);

--
-- Indexes for table `account_tbl`
--
ALTER TABLE `account_tbl`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `activity_tbl`
--
ALTER TABLE `activity_tbl`
  ADD PRIMARY KEY (`activity_number`);

--
-- Indexes for table `penalty_tbl`
--
ALTER TABLE `penalty_tbl`
  ADD PRIMARY KEY (`penalty_id`);

--
-- Indexes for table `studentlog_tbl`
--
ALTER TABLE `studentlog_tbl`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `students_tbl`
--
ALTER TABLE `students_tbl`
  ADD PRIMARY KEY (`id_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts_tbl`
--
ALTER TABLE `accounts_tbl`
  MODIFY `acc_no` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `activity_tbl`
--
ALTER TABLE `activity_tbl`
  MODIFY `activity_number` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penalty_tbl`
--
ALTER TABLE `penalty_tbl`
  MODIFY `penalty_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `studentlog_tbl`
--
ALTER TABLE `studentlog_tbl`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
