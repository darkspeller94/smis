package sams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class penaltyframe extends javax.swing.JFrame {

    Connection con = new DbConnection().con();
    ResultSet rs;
    PreparedStatement pst;
    String maxxx;

    public penaltyframe() {
        initComponents();
        maxCnt();
    }
    void maxCnt() {
        try {
            pst = con.prepareStatement("SELECT MAX(acc_no)MID from account_tbl");
            rs = pst.executeQuery();
            if (rs.next()) {
                int max = rs.getInt("mid");
                max++;
                maxxx = Integer.toString(max);
                q1.setText(maxxx);
            } else {
                maxxx = "1";
                q1.setText(maxxx);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        q1 = new javax.swing.JTextField();
        q2 = new javax.swing.JTextField();
        q4 = new javax.swing.JTextField();
        q3 = new javax.swing.JTextField();
        q5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        q6 = new javax.swing.JTextField();
        back = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        q7 = new javax.swing.JTextField();
        markassettelled = new javax.swing.JButton();
        sendsms = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Project number:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(50, 130, 130, 30);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID Number:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(50, 170, 130, 30);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Description:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(50, 250, 130, 30);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Full Name:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(50, 290, 130, 30);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Status:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(50, 370, 130, 30);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Green Project");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(0, 0, 510, 60);
        getContentPane().add(q1);
        q1.setBounds(170, 130, 270, 30);
        getContentPane().add(q2);
        q2.setBounds(170, 170, 270, 30);
        getContentPane().add(q4);
        q4.setBounds(170, 250, 270, 30);
        getContentPane().add(q3);
        q3.setBounds(170, 210, 270, 30);
        getContentPane().add(q5);
        q5.setBounds(170, 290, 270, 30);

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Activity Number:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(50, 210, 130, 30);
        getContentPane().add(q6);
        q6.setBounds(170, 330, 270, 30);

        back.setText("BACK");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        getContentPane().add(back);
        back.setBounds(340, 430, 120, 23);

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Program:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(50, 330, 130, 30);

        q7.setText("UNSETTLED");
        getContentPane().add(q7);
        q7.setBounds(170, 370, 270, 30);

        markassettelled.setText("MARK AS SETTLED");
        markassettelled.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                markassettelledActionPerformed(evt);
            }
        });
        getContentPane().add(markassettelled);
        markassettelled.setBounds(170, 430, 150, 23);

        sendsms.setText("SEND SMS");
        sendsms.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendsmsActionPerformed(evt);
            }
        });
        getContentPane().add(sendsms);
        sendsms.setBounds(50, 430, 110, 23);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sams/pictures/neust logo1.png"))); // NOI18N
        jLabel10.setText("jLabel10");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(-30, 10, 210, 110);

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sams/pictures/penalty 1.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(0, 0, 510, 490);

        setSize(new java.awt.Dimension(510, 489));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        new MainMenu_Admin().setVisible(true);
        MainMenu_Admin.penalty_tbl.setVisible(true);
        dispose();
    }//GEN-LAST:event_backActionPerformed

    private void markassettelledActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_markassettelledActionPerformed
        try{
            pst = con.prepareStatement("Delete from penalty_tbl where penalty_id='"+q1.getText()+"'");
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to mark as settled?","Mark as Settled?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
            if (x == 0){
                pst.execute();
                JOptionPane.showMessageDialog(null, "Successfully Marked as Settled");
                new MainMenu_Admin().setVisible(true);
                dispose();
            }
            
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_markassettelledActionPerformed

    private void sendsmsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendsmsActionPerformed
        try{
            pst = con.prepareStatement("Select * from students_tbl where id_number='"+q2.getText()+"'");
            rs = pst.executeQuery();
            if(rs.next()){
                String cn = rs.getString("contact_number");
                String fn = rs.getString("first_name");
                String message = "Good day "+fn+", You have penalty to settle. Please go to registar immediately. Thank you";
        String phone = cn;
        String username = "root";
        String password = "password";
        String address = "http://192.168.1.22";
        String port = "8090";
        
        try {
            URL url = new URL(
                    address+":"+port+"/SendSMS?username="+username+"&password="+password+
                            "&phone="+phone+"&message="+URLEncoder.encode(message, "ISO-8859-1"));
            URLConnection connection = url.openConnection();
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputline;
            while((inputline = bufferedreader.readLine())!=null){
                System.out.print(inputline);
            }
            bufferedreader.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);
        }
            }
        }
        catch(Exception e){
            
        }
        
    }//GEN-LAST:event_sendsmsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(penaltyframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(penaltyframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(penaltyframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(penaltyframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new penaltyframe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton markassettelled;
    public static javax.swing.JTextField q1;
    public static javax.swing.JTextField q2;
    public static javax.swing.JTextField q3;
    public static javax.swing.JTextField q4;
    public static javax.swing.JTextField q5;
    public static javax.swing.JTextField q6;
    public static javax.swing.JTextField q7;
    private javax.swing.JButton sendsms;
    // End of variables declaration//GEN-END:variables
}
