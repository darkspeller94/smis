
package sams;

import com.google.zxing.EncodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static sams.StudentFrame.idnum;
import static sams.qrcode.createQRCode;
import static sams.qrcode.loadImage;
import static sams.qrcode.readQRCode;


public class StudentFrame extends javax.swing.JFrame {

Connection con = new DbConnection().con();
ResultSet rs;
PreparedStatement pst,pst1;
 String qrCodeData;
    String filePath = "QRcode.png";
    String charset = "UTF-8"; // or "ISO-8859-1"
    File imageFile = null;
    public StudentFrame() {
        initComponents();
    // tampilQrCode(); 
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        asd = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        mname = new javax.swing.JTextField();
        idnum = new javax.swing.JTextField();
        fname = new javax.swing.JTextField();
        prog = new javax.swing.JTextField();
        sec = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        years = new javax.swing.JComboBox<>();
        add = new javax.swing.JTextField();
        guardiancontact = new javax.swing.JTextField();
        cnum = new javax.swing.JTextField();
        guardian = new javax.swing.JTextField();
        back = new javax.swing.JButton();
        save = new javax.swing.JButton();
        updates = new javax.swing.JButton();
        edit = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        lname = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sams/pictures/neust logo1.png"))); // NOI18N
        jLabel7.setText("jLabel7");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(520, 10, 190, 100);

        jLabel1.setBackground(new java.awt.Color(255, 255, 51));
        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Student Information");
        jLabel1.setOpaque(true);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 720, 50);

        jLabel2.setText("Middle Name:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(460, 90, 140, 40);

        jLabel3.setText("ID Number:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 50, 120, 40);

        jLabel4.setText("Program:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(460, 160, 140, 40);

        jLabel5.setText("First Name:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(250, 90, 140, 40);

        jLabel6.setText("Last Name:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(40, 90, 140, 40);

        asd.setText("Year:");
        getContentPane().add(asd);
        asd.setBounds(40, 160, 140, 40);

        jLabel8.setText("Section:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(250, 160, 140, 40);
        getContentPane().add(mname);
        mname.setBounds(460, 120, 170, 30);
        getContentPane().add(idnum);
        idnum.setBounds(120, 60, 300, 30);
        getContentPane().add(fname);
        fname.setBounds(250, 120, 170, 30);
        getContentPane().add(prog);
        prog.setBounds(460, 190, 170, 30);
        getContentPane().add(sec);
        sec.setBounds(250, 190, 170, 30);

        jLabel9.setText("Address:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(40, 300, 140, 40);

        jLabel10.setText("Guardian Name:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(250, 230, 140, 40);

        jLabel11.setText("Guardian Contact No:");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(460, 230, 140, 40);

        years.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1st", "2nd", "3rd", "4th" }));
        getContentPane().add(years);
        years.setBounds(40, 190, 170, 30);
        getContentPane().add(add);
        add.setBounds(110, 310, 520, 30);
        getContentPane().add(guardiancontact);
        guardiancontact.setBounds(460, 260, 170, 30);
        getContentPane().add(cnum);
        cnum.setBounds(40, 260, 170, 30);

        guardian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardianActionPerformed(evt);
            }
        });
        getContentPane().add(guardian);
        guardian.setBounds(250, 260, 170, 30);

        back.setText("BACK");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        getContentPane().add(back);
        back.setBounds(570, 360, 90, 30);

        save.setText("SAVE");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });
        getContentPane().add(save);
        save.setBounds(110, 350, 170, 40);

        updates.setText("UPDATE");
        updates.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatesActionPerformed(evt);
            }
        });
        getContentPane().add(updates);
        updates.setBounds(110, 350, 170, 40);

        edit.setText("EDIT");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });
        getContentPane().add(edit);
        edit.setBounds(330, 360, 90, 30);

        delete.setText("DELETE");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        getContentPane().add(delete);
        delete.setBounds(450, 360, 90, 30);
        getContentPane().add(lname);
        lname.setBounds(40, 120, 170, 30);

        jLabel12.setText("Contact Number:");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(40, 230, 140, 40);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sams/pictures/students.png"))); // NOI18N
        getContentPane().add(jLabel13);
        jLabel13.setBounds(0, 0, 720, 410);

        setSize(new java.awt.Dimension(718, 407));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed

        StudentFrame.lname.setEnabled(true);
        StudentFrame.fname.setEnabled(true);
        StudentFrame.mname.setEnabled(true);
        StudentFrame.years.setEnabled(true);
        StudentFrame.sec.setEnabled(true);
        StudentFrame.prog.setEnabled(true);
        StudentFrame.cnum.setEnabled(true);
        StudentFrame.guardian.setEnabled(true);
        StudentFrame.guardiancontact.setEnabled(true);
        StudentFrame.add.setEnabled(true);
    }//GEN-LAST:event_editActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        new MainMenu_Admin().setVisible(true);
        MainMenu_Admin.students_frame.setVisible(true);
        dispose();
    }//GEN-LAST:event_backActionPerformed

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed

       
           
           int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to SAVE?", "SAVE?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
             try{
           pst = con.prepareStatement("Insert into students_tbl values (?,?,?,?,?,?,?,?,?)");
           pst.setString(1, idnum.getText());
           pst.setString(2, lname.getText());
           pst.setString(3, fname.getText());
           pst.setString(4, mname.getText());
           pst.setString(5,(String) years.getSelectedItem());
           pst.setString(6, sec.getText());
           pst.setString(7, prog.getText());
           pst.setString(8, cnum.getText());
           pst.setString(9, add.getText());
           pst.executeUpdate();
           
           pst1 = con.prepareStatement("Insert into guardian_tbl values (?,?,?)");
           pst1.setString(1, idnum.getText());
           pst1.setString(2, guardian.getText());
           pst1.setString(3, guardiancontact.getText());
           //pst.executeUpdate();
           tampilQrCode();
            pst1.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Added Successfully");
//            new MainMenu_Admin().setVisible(true);
//            MainMenu_Admin.students_frame.setVisible(true);
             //dispose();
             qrcode q= new qrcode();
            q.setVisible(true);
            
         }
        catch(HeadlessException | SQLException e){
            e.printStackTrace();
        }   
        }
        
        
    }//GEN-LAST:event_saveActionPerformed

    private void updatesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatesActionPerformed
        try{
           pst = con.prepareStatement("update students_tbl set last_name=?,first_name=?,middle_name=?,year_level=?,section=?,program=?,contact_number=?,address=? where id_number='"+idnum.getText()+"'");
           pst.setString(1, lname.getText());
           pst.setString(2, fname.getText());
           pst.setString(3, mname.getText());
           pst.setString(4, (String) years.getSelectedItem());
           pst.setString(5, sec.getText());
           pst.setString(6, prog.getText());
           pst.setString(7, cnum.getText());
           pst.setString(8, add.getText());
           int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update?", "UPDATE?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Updated Successfully");
            new MainMenu_Admin().setVisible(true);
             MainMenu_Admin.students_frame.setVisible(true);
            dispose();
        }
        }
        catch(Exception e){
            e.printStackTrace();
        }
         try{
           pst = con.prepareStatement("update guardian_tbl SET guardian_name=?,guardian_number=? where students_tbl.id_number='"+idnum.getText()+"'");
           pst.setString(1, guardian.getText());
           pst.setString(2, guardiancontact.getText());
           pst.executeUpdate();
        }
        catch(Exception e){
            
        }
    }//GEN-LAST:event_updatesActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        try{
            pst = con.prepareStatement("DELETE FROM students_tbl WHERE id_number = '"+idnum.getText()+"'");
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete?", "DELETE?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
            pst.execute();
            JOptionPane.showMessageDialog(null, "Deleted Successfully");
            new MainMenu_Admin().setVisible(true);
            MainMenu_Admin.students_frame.setVisible(true);
            dispose();
        }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_deleteActionPerformed

    private void guardianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardianActionPerformed
     
    }//GEN-LAST:event_guardianActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StudentFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StudentFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StudentFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StudentFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StudentFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField add;
    private javax.swing.JLabel asd;
    private javax.swing.JButton back;
    public static javax.swing.JTextField cnum;
    public static javax.swing.JButton delete;
    public static javax.swing.JButton edit;
    public static javax.swing.JTextField fname;
    public static javax.swing.JTextField guardian;
    public static javax.swing.JTextField guardiancontact;
    public static javax.swing.JTextField idnum;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public static javax.swing.JTextField lname;
    public static javax.swing.JTextField mname;
    public static javax.swing.JTextField prog;
    public static javax.swing.JButton save;
    public static javax.swing.JTextField sec;
    public static javax.swing.JButton updates;
    public static javax.swing.JComboBox<String> years;
    // End of variables declaration//GEN-END:variables
private void tampilQrCode(){
        StudentFrame.idnum.getText();

        Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new EnumMap<>(EncodeHintType.class);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        try {
            try {
                createQRCode(idnum.getText(), filePath, charset, hintMap, 250, 250);
            } catch (WriterException ex) {
                Logger.getLogger(StudentFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } catch (IOException ex) {
            Logger.getLogger(StudentFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("QR Code image created successfully!");

   
    }
}
