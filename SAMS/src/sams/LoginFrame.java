package sams;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class LoginFrame extends javax.swing.JFrame {

    Connection con = new DbConnection().con();
    ResultSet rs;
    PreparedStatement pst;

    public LoginFrame() {
        initComponents();
        pword.setFocusable(true);
        pword.requestFocus();
    }

    public void clear() {
        uname.setText(null);
        pword.setText(null);
    }
int a=0;
    void login() {
        
        
        try {
            String user = uname.getText();
            String pass = pword.getText();
            pst = con.prepareStatement("Select * from account_tbl where user='" + user + "' and password='" + pass + "'");
            rs = pst.executeQuery();
            if (rs.next()) {
                String role = rs.getString("role");
                JOptionPane.showMessageDialog(null, "Access granted.");
                if (role.equals("ADMIN")) {
                    new MainMenu_Admin().setVisible(true);
                    dispose();
                } else if (role.equals("STUDENT")) {
                    try {
                        pst = con.prepareStatement("Select * from activity_tbl WHERE DATE(date_of_activity) = DATE(NOW())");
                        rs = pst.executeQuery();
                        if (rs.next()) {
                            String actno = rs.getString("activity_number");
                            try {
                                pst = con.prepareStatement("Select * from account_tbl where user='" + user + "'");
                                rs = pst.executeQuery();
                                if (rs.next()) {
                                    String accno = rs.getString("acc_no");
                                    pst = con.prepareStatement("Select * from accounts_tbl where acc_no='" + accno + "'");
                                    rs = pst.executeQuery();
                                    if (rs.next()) {
                                        String idnoo = rs.getString("id_number");
                                        pst = con.prepareStatement("Update penalty_tbl set status='Present',stat='Settled' where activity_number='" + actno + "'"
                                                + "and id_number='" + idnoo + "'");
                                        pst.executeUpdate();
                                    }

                                } else {

                                }
                            } catch (Exception e) {

                            }

                        } else {
                            JOptionPane.showMessageDialog(null, "There's no event today");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (role.equals("OFFICER")) {
                    new MainMenu_Admin().setVisible(true);
                    MainMenu_Admin.officers.setVisible(false);
                    MainMenu_Admin.activities.setVisible(false);
                    MainMenu_Admin.account.setVisible(false);
                    dispose();
                }
            } else {
              a++;
              pst = con.prepareStatement("Select * from account_tbl where user='" + user + "'");
              rs=pst.executeQuery();
             if(rs.next()){
                 JOptionPane.showMessageDialog(null, "Password is Incorrect.");
             }
             pst = con.prepareStatement("Select * from account_tbl where password='" + pass + "'");
              rs=pst.executeQuery();
             if(rs.next()){
                 JOptionPane.showMessageDialog(null, "Username is Incorrect.");
             }
              if(a==1){
          
             }else if(a==2){
                 
             }else if(a==3){
                
             }else if(a==4){
                 JOptionPane.showMessageDialog(null, "Last Attempt Left");
             }
             else{
              pst = con.prepareStatement("Select * from account_tbl where security_code='" +    JOptionPane.showInputDialog(null, "Enter Code") + "'");
            rs = pst.executeQuery();
            if(rs.next()){
                JOptionPane.showMessageDialog(null, "LOgin Again");
                a=0;
            
            }else{
                System.exit(0);
            }
            
             }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pword.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        clr = new javax.swing.JButton();
        uname = new javax.swing.JTextField();
        pword = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        checkpass = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setLayout(null);

        jButton1.setText("LOGIN");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(350, 370, 140, 40);

        jButton2.setText("EXIT");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(660, 370, 140, 40);

        clr.setText("CLEAR");
        clr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clrActionPerformed(evt);
            }
        });
        jPanel1.add(clr);
        clr.setBounds(510, 370, 140, 40);

        uname.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(uname);
        uname.setBounds(490, 220, 240, 40);

        pword.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        pword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        pword.setBorder(null);
        pword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pwordActionPerformed(evt);
            }
        });
        jPanel1.add(pword);
        pword.setBounds(490, 270, 240, 40);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("PASSWORD :");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(350, 270, 130, 40);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("(GREEN PROJECT)");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(240, 130, 720, 40);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("USERNAME :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(350, 220, 130, 40);

        checkpass.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        checkpass.setText("(show password)");
        checkpass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkpassActionPerformed(evt);
            }
        });
        jPanel1.add(checkpass);
        checkpass.setBounds(600, 320, 131, 27);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("WELCOME TO ");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(280, 30, 610, 50);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 26)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("NEUST STUDENT ACTIVITIES MONITORING SYSTEM");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(200, 70, 750, 50);

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\SAMS\\src\\sams\\pictures\\login2.png")); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 930, 460);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(930, 460));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void pwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pwordActionPerformed

    }//GEN-LAST:event_pwordActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        login();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void clrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clrActionPerformed
        clear();
    }//GEN-LAST:event_clrActionPerformed

    private void checkpassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkpassActionPerformed
        // TODO add your handling code here:
        if (checkpass.isSelected()) {
            pword.setEchoChar((char) 0);
        } else {
            pword.setEchoChar('*');
        }
    }//GEN-LAST:event_checkpassActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox checkpass;
    private javax.swing.JButton clr;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField pword;
    private javax.swing.JTextField uname;
    // End of variables declaration//GEN-END:variables
}
