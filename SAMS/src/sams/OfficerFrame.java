
package sams;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;


public class OfficerFrame extends javax.swing.JFrame {
Connection con = new DbConnection().con();
ResultSet rs;
PreparedStatement pst;
String maxxx;
    public OfficerFrame() {
        initComponents();
        maxCnt();
    }
void maxCnt(){
////    try{
////            pst = con.prepareStatement("SELECT MAX(officer_number)MID from officer_tbl");
////            rs = pst.executeQuery();
////            if(rs.next()){
////                int max = rs.getInt("mid");
////                max++;
////                maxxx = Integer.toString(max);
////                q1.setText(maxxx);
////            }
////            else{
////                maxxx = "1";
////                q1.setText(maxxx);
////            }
////        }
//        catch(Exception e){
//            e.printStackTrace();
//        }
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        q2 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        q1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Officer Information");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(20, 10, 250, 50);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("POSITION:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(130, 50, 190, 40);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("ID Number:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(120, 110, 140, 40);

        jButton1.setText("BACK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(600, 20, 90, 30);

        jButton2.setText("SAVE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(270, 180, 180, 40);

        jButton3.setText("UPDATE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(270, 180, 180, 40);

        jButton5.setText("DELETE");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton5);
        jButton5.setBounds(510, 20, 90, 30);
        getContentPane().add(q2);
        q2.setBounds(250, 120, 330, 30);

        jButton6.setText("EDIT");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6);
        jButton6.setBounds(420, 20, 90, 30);

        q1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ADMIN", "OFFICER", "STUDENT" }));
        getContentPane().add(q1);
        q1.setBounds(250, 60, 330, 30);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sams/pictures/officer.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 710, 240);

        setSize(new java.awt.Dimension(704, 235));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
    q1.setEnabled(true);
    q2.setEnabled(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            pst = con.prepareStatement("Select * from students_tbl where id_number='"+q2.getText()+"'");
            rs = pst.executeQuery();
            if(rs.next()){
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            pst = con.prepareStatement("INSERT into officer_tbl values (?,?,?)");
            pst.setString(2, q1.getSelectedItem().toString());
            pst.setString(1, q2.getText());
            pst.setString(3, dateFormat.format(date));
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to SAVE?", "SAVE?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
            
                pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Added Successfully");
            new MainMenu_Admin().setVisible(true);
            MainMenu_Admin.officer_frame.setVisible(true);
            dispose();
            
            
            
        }
                }
            else{
                JOptionPane.showMessageDialog(null, "Student not Found");
            }
            
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try{
            pst = con.prepareStatement("Select * from students_tbl where id_number='"+q2.getText()+"'");
            rs = pst.executeQuery();
            if(rs.next()){
                pst = con.prepareStatement("Update officer_tbl SET id_number='"+q2.getText()+"' where position='"+q1.getSelectedItem()+"'");
            
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update?", "Update?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Updated Successfully");
            new MainMenu_Admin().setVisible(true);
            dispose();
        }
            }
            else{
                JOptionPane.showMessageDialog(null, "Student Cannot Found");
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new MainMenu_Admin().setVisible(true);
        MainMenu_Admin.officer_frame.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try{
            pst = con.prepareStatement("Delete from officer_tbl where id_number='"+q2.getText()+"'");
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete?", "Delete?",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_OPTION);
        if(x == 0)
        {
             pst.execute();
             JOptionPane.showMessageDialog(null, "Deleted");
             new MainMenu_Admin().setVisible(true);
            dispose();
        }
           
        }
        catch(Exception e){
            
        }
    }//GEN-LAST:event_jButton5ActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OfficerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OfficerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OfficerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OfficerFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OfficerFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    public static javax.swing.JButton jButton2;
    public static javax.swing.JButton jButton3;
    public static javax.swing.JButton jButton5;
    public static javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    public static javax.swing.JComboBox<String> q1;
    public static javax.swing.JTextField q2;
    // End of variables declaration//GEN-END:variables
}
