package sams;

import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.proteanit.sql.DbUtils;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import static sams.StudentFrame.idnum;
import static sams.qrcode.createQRCode;

public class MainMenu_Admin extends javax.swing.JFrame {

    String filePath = "QRcode.png";
    String charset = "UTF-8"; // or "ISO-8859-1"
    File imageFile = null;
    Connection con = new DbConnection().con();
    ResultSet rs;
    PreparedStatement pst;
    int stud_view = 0;
    int officer_view = 0;
    int activity_view = 0;
    int account_view = 0;
    int penalty_view = 0;
    String maxxx;

    public MainMenu_Admin() {
        initComponents();
        students_frame.setVisible(false);
        officer_frame.setVisible(false);
        record_frame.setVisible(false);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(false);
        student_tbl();
        officer_tbl();
        activities_tbl();
        account_tbl();
        penalty_tbl();
//        maxmid();
    }

    static public class HeaderColor extends DefaultTableCellRenderer {

        public HeaderColor() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//            setBackground(new Color(102,102,102));
            setBackground(Color.darkGray);
            setForeground(Color.WHITE);
            setPreferredSize(new Dimension(0, 30));
            setFont(new Font("Barlow", Font.BOLD, 14));
            setHorizontalAlignment(JLabel.LEADING);
            return this;
        }

    }

    public static void ptbl() {
        penalty_table.getTableHeader().setDefaultRenderer(new HeaderColor());
        penalty_table.getColumnModel().getColumn(0).setHeaderValue("PENALTY ID");
        penalty_table.getColumnModel().getColumn(1).setHeaderValue("ACTIVITY NAME");
        penalty_table.getColumnModel().getColumn(2).setHeaderValue("ID NUMBER");
        penalty_table.getColumnModel().getColumn(3).setHeaderValue("STATUS");
        penalty_table.getColumnModel().getColumn(4).setHeaderValue("REMARKS");
        penalty_table.getColumnModel().getColumn(0).setPreferredWidth(150);
        penalty_table.getColumnModel().getColumn(1).setPreferredWidth(150);
        penalty_table.getColumnModel().getColumn(2).setPreferredWidth(150);
        penalty_table.getColumnModel().getColumn(3).setPreferredWidth(150);
        penalty_table.getColumnModel().getColumn(4).setPreferredWidth(150);

    }

    public static void stbl() {
        student_table.getTableHeader().setDefaultRenderer(new HeaderColor());
        student_table.getColumnModel().getColumn(0).setHeaderValue("ID NUMBER");
        student_table.getColumnModel().getColumn(1).setHeaderValue("LAST NAME");
        student_table.getColumnModel().getColumn(2).setHeaderValue("FIRST NAME");
        student_table.getColumnModel().getColumn(3).setHeaderValue("MIDDLE NAME");
        student_table.getColumnModel().getColumn(4).setHeaderValue("YEAR LEVEL");
        student_table.getColumnModel().getColumn(5).setHeaderValue("SECTION");
        student_table.getColumnModel().getColumn(6).setHeaderValue("PROGRAM");
        student_table.getColumnModel().getColumn(7).setHeaderValue("ADDRESS");
        student_table.getColumnModel().getColumn(8).setHeaderValue("GUARDIAN");
        student_table.getColumnModel().getColumn(9).setHeaderValue("GUARDIAN NUMBER");
        student_table.getColumnModel().getColumn(0).setPreferredWidth(60);
        student_table.getColumnModel().getColumn(1).setPreferredWidth(70);
        student_table.getColumnModel().getColumn(2).setPreferredWidth(70);
        student_table.getColumnModel().getColumn(3).setPreferredWidth(70);
        student_table.getColumnModel().getColumn(4).setPreferredWidth(60);
        student_table.getColumnModel().getColumn(5).setPreferredWidth(40);
        student_table.getColumnModel().getColumn(6).setPreferredWidth(60);
        student_table.getColumnModel().getColumn(7).setPreferredWidth(170);
        student_table.getColumnModel().getColumn(8).setPreferredWidth(150);
        student_table.getColumnModel().getColumn(9).setPreferredWidth(40);

    }

    public static void atvtbl() {
        activity_table.getTableHeader().setDefaultRenderer(new HeaderColor());
        activity_table.getColumnModel().getColumn(0).setHeaderValue("ACTIVITY NUMBER");
        activity_table.getColumnModel().getColumn(1).setHeaderValue("NAME");
        activity_table.getColumnModel().getColumn(2).setHeaderValue("DESCRIPTION");
        activity_table.getColumnModel().getColumn(3).setHeaderValue("ACTIVITY DATE");
        activity_table.getColumnModel().getColumn(4).setHeaderValue("END DATE");
        activity_table.getColumnModel().getColumn(5).setHeaderValue("TIME START");
        activity_table.getColumnModel().getColumn(6).setHeaderValue("TIME END");
        activity_table.getColumnModel().getColumn(7).setHeaderValue("TIME END");
        activity_table.getColumnModel().getColumn(0).setPreferredWidth(150);
        activity_table.getColumnModel().getColumn(1).setPreferredWidth(150);
        activity_table.getColumnModel().getColumn(2).setPreferredWidth(120);
        activity_table.getColumnModel().getColumn(3).setPreferredWidth(120);
        activity_table.getColumnModel().getColumn(4).setPreferredWidth(140);
        activity_table.getColumnModel().getColumn(5).setPreferredWidth(170);
        activity_table.getColumnModel().getColumn(6).setPreferredWidth(170);
        activity_table.getColumnModel().getColumn(7).setPreferredWidth(170);

    }

    public static void atbl() {
        account_table.getTableHeader().setDefaultRenderer(new HeaderColor());
        account_table.getColumnModel().getColumn(0).setHeaderValue("ACCOUNT NUMBER");
        account_table.getColumnModel().getColumn(1).setHeaderValue("ID NUMBER");
        account_table.getColumnModel().getColumn(0).setPreferredWidth(150);
        account_table.getColumnModel().getColumn(1).setPreferredWidth(150);

    }

    public static void otbl() {
        officer_table.getTableHeader().setDefaultRenderer(new HeaderColor());
        officer_table.getColumnModel().getColumn(0).setHeaderValue("ID NUMBER");
        officer_table.getColumnModel().getColumn(1).setHeaderValue("POSITION");
        officer_table.getColumnModel().getColumn(2).setHeaderValue("DATE CREATED");
        officer_table.getColumnModel().getColumn(0).setPreferredWidth(150);
        officer_table.getColumnModel().getColumn(1).setPreferredWidth(150);
        officer_table.getColumnModel().getColumn(2).setPreferredWidth(150);

    }

//    void tbl_design(){
//        jTable1.getTableHeader().setFont(new Font("Serge UI",Font.BOLD,12));
//        jTable1.getTableHeader().setOpaque(false);
//        jTable1.getTableHeader().setBackground(new Color(32,136,203));
//        jTable1.getTableHeader().setForeground(new Color(255,255,255));
//        jTable1.setRowHeight(25);
//    }
//     
    public void student_tbl() {
        try {

//            pst = con.prepareStatement("SELECT * from students_tbl inner join guardian_tbl on students_tbl.id_number = guardian_tbl.id_number");
//            rs = pst.executeQuery();
//            student_table.setModel(DbUtils.resultSetToTableModel(rs));
//            stbl();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sams", "root", "");
            String sql = ("SELECT students_tbl.id_number as'ID NUMBER',last_name AS 'LASTNAME',first_name AS 'FIRSTNAME'"
                    + ",middle_name AS 'MIDDLE NAME',students_tbl.year_level as'YEAR LEVEL',students_tbl.section as'SECTION',"
                    + "students_tbl.program as'PROGRAM',students_tbl.address as'ADDRESS'"
                    + ",guardian_tbl.guardian_name as'GUARDIAN',guardian_tbl.guardian_number as'GUARDIAN NUMBER'"
                    + " from students_tbl inner join guardian_tbl on students_tbl.id_number = guardian_tbl.id_number order by last_name asc");
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            student_table.setModel(DbUtils.resultSetToTableModel(rs));
            stbl();
//    student_table.getColumnModel().getColumn(0).setMinWidth(30); student_table.getColumnModel().getColumn(0).setMaxWidth(30);
//    student_table.getColumnModel().getColumn(1).setMinWidth(150); student_table.getColumnModel().getColumn(1).setMaxWidth(150);    
//    student_table.getColumnModel().getColumn(2).setMinWidth(20); student_table.getColumnModel().getColumn(2).setMaxWidth(20);
//    student_table.getColumnModel().getColumn(3).setMinWidth(20); student_table.getColumnModel().getColumn(3).setMaxWidth(20);    
//    student_table.getColumnModel().getColumn(5).setMinWidth(20); student_table.getColumnModel().getColumn(5).setMaxWidth(20);      
//    student_table.getColumnModel().getColumn(6).setMinWidth(200); student_table.getColumnModel().getColumn(6).setMaxWidth(200);
//    student_table.getColumnModel().getColumn(7).setMinWidth(150); student_table.getColumnModel().getColumn(7).setMaxWidth(150);      
//    student_table.getColumnModel().getColumn(8).setMinWidth(160); student_table.getColumnModel().getColumn(8).setMaxWidth(160);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void maxmid() {
        try {
            pst = con.prepareStatement("Select MAX(penalty_id)MID from penalty_tbl");
            rs = pst.executeQuery();
            if (rs.next()) {
                int max = rs.getInt("mid");
                max++;
                maxxx = Integer.toString(max);

            } else {
                maxxx = "1";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void penalty_tbl() {
        try {
            pst = con.prepareStatement("Select penalty_tbl.penalty_id,activity_tbl.activity_name,penalty_tbl.id_number,penalty_tbl.status,penalty_tbl.remarks from penalty_tbl JOIN activity_tbl where penalty_tbl.status='Absent' and penalty_tbl.remarks='Unsettled' and activity_tbl.activity_number = penalty_tbl.activity_number");
            rs = pst.executeQuery();
            penalty_table.setModel(DbUtils.resultSetToTableModel(rs));
            ptbl();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void activities_tbl() {
        try {
            pst = con.prepareStatement("Select activity_number AS 'Activity Number',activity_Name AS 'Name',"
                    + "description AS 'Description',date_of_activity AS 'Activity Date',"
                    + "end_date AS 'End Date',time_start AS 'Start time',time_end AS 'Time End',time AS 'Time' from activity_tbl"
                    + " ORDER BY date_of_activity asc");
            rs = pst.executeQuery();
            activity_table.setModel(DbUtils.resultSetToTableModel(rs));
            atvtbl();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public String time_c0mpute(String tmestart, String tmeend) {
//        String hr1, min1, hr2, min2, hrt, mint;
//        if (tmestart.length() == 6 && tmeend.length() == 6) {
//            hr1 = tmestart.substring(0, 1);
//            min1 = tmestart.substring(2, 4);
//            hr2 = tmeend.substring(0, 1);
//            min2 = tmeend.substring(2, 4);
//            if (tmestart.substring(4, 6).equals("pm") && tmeend.substring(4, 6).equals("pm") || (tmestart.substring(4, 6).equals("am") && tmeend.substring(4, 6).equals("am"))) {
//                hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            } else if ((tmestart.substring(4, 6).equals("am") && tmeend.substring(4, 6).equals("pm")) || (tmestart.substring(4, 6).equals("pm") && tmeend.substring(4, 6).equals("am"))) {
//                hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            }
//
//        } else if (tmestart.length() == 6 && tmeend.length() == 7) {
//            hr1 = tmestart.substring(0, 1);
//            min1 = tmestart.substring(2, 4);
//            hr2 = tmeend.substring(0, 2);
//            min2 = tmeend.substring(3, 5);
//            hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
//            mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//            if (tmestart.substring(4, 6).equals("pm") && tmeend.substring(5, 7).equals("pm") || (tmestart.substring(4, 6).equals("am") && tmeend.substring(5, 7).equals("am"))) {
//                hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            } else if ((tmestart.substring(4, 6).equals("am") && tmeend.substring(5, 7).equals("pm")) || (tmestart.substring(4, 6).equals("pm") && tmeend.substring(5, 7).equals("am"))) {
//                hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            }
//        } else if (tmestart.length() == 7 && tmeend.length() == 7) {
//            hr1 = tmestart.substring(0, 2);
//            min1 = tmestart.substring(3, 5);
//            hr2 = tmeend.substring(0, 2);
//            min2 = tmeend.substring(3, 5);
//            hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
//            mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//            if (tmestart.substring(5, 7).equals("pm") && tmeend.substring(5, 7).equals("pm") || (tmestart.substring(5, 7).equals("am") && tmeend.substring(5, 7).equals("am"))) {
//                hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            } else if ((tmestart.substring(5, 7).equals("am") && tmeend.substring(5, 7).equals("pm")) || (tmestart.substring(5, 7).equals("pm") && tmeend.substring(5, 7).equals("am"))) {
//                hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
//                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
//                if (Integer.parseInt(mint) < 0) {
//                    hrt = String.valueOf(Integer.parseInt(hrt) - 1);
//                    mint = String.valueOf(Integer.parseInt(mint) + 60);
//                    return hrt + ":" + mint;
//                } else {
//                    return hrt + ":" + mint;
//                }
//            }
//        }
//        return "";
//    }

    public void officer_tbl() {
        try {
            pst = con.prepareStatement("Select * from officer_tbl");
            rs = pst.executeQuery();
            officer_table.setModel(DbUtils.resultSetToTableModel(rs));
            otbl();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void account_tbl() {
        try {
            pst = con.prepareStatement("Select * from accounts_tbl");
            rs = pst.executeQuery();
            account_table.setModel(DbUtils.resultSetToTableModel(rs));
            atbl();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewactinfo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        students_frame = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        student_table = new javax.swing.JTable();
        n6 = new javax.swing.JButton();
        n5 = new javax.swing.JButton();
        viewstudinfo = new javax.swing.JButton();
        n2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        search = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        officer_frame = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        officer_table = new javax.swing.JTable();
        viewoffinfo = new javax.swing.JButton();
        n4 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        search1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        penalty_tbl = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        penalty_table = new javax.swing.JTable();
        sendsms = new javax.swing.JButton();
        viewpenalty = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        search3 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        accounts_tbl = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        account_table = new javax.swing.JTable();
        viewacctim = new javax.swing.JButton();
        n1 = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        search4 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        record_frame = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        activity_table = new javax.swing.JTable();
        n3 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        search2 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        report_frame = new javax.swing.JPanel();
        printall = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        account = new javax.swing.JButton();
        penalty = new javax.swing.JButton();
        reports = new javax.swing.JButton();
        activities = new javax.swing.JButton();
        officers = new javax.swing.JButton();
        student = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        viewactinfo.setBackground(new java.awt.Color(255, 255, 255));
        viewactinfo.setText("VIEW ACTIVITY INFORMATION");
        viewactinfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewactinfoActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setPreferredSize(new java.awt.Dimension(982, 587));
        jPanel1.setLayout(null);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\photoshop\\logoneust.png")); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(-10, -50, 200, 280);

        students_frame.setBackground(new java.awt.Color(0, 0, 0));
        students_frame.setLayout(null);

        student_table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        student_table.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        student_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        student_table.setIntercellSpacing(new java.awt.Dimension(0, 0));
        student_table.setRowHeight(25);
        student_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                student_tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(student_table);

        students_frame.add(jScrollPane1);
        jScrollPane1.setBounds(40, 110, 1260, 490);

        n6.setBackground(new java.awt.Color(255, 255, 255));
        n6.setText("SCANNER");
        n6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n6ActionPerformed(evt);
            }
        });
        students_frame.add(n6);
        n6.setBounds(170, 600, 230, 30);

        n5.setBackground(new java.awt.Color(255, 255, 255));
        n5.setText("VIEW QR CODE");
        n5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n5ActionPerformed(evt);
            }
        });
        students_frame.add(n5);
        n5.setBounds(990, 600, 190, 30);

        viewstudinfo.setBackground(new java.awt.Color(255, 255, 255));
        viewstudinfo.setText("VIEW STUDENT INFORMATION");
        viewstudinfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewstudinfoActionPerformed(evt);
            }
        });
        students_frame.add(viewstudinfo);
        viewstudinfo.setBounds(600, 600, 380, 30);

        n2.setBackground(new java.awt.Color(255, 255, 255));
        n2.setText("NEW");
        n2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n2ActionPerformed(evt);
            }
        });
        students_frame.add(n2);
        n2.setBounds(410, 600, 170, 30);

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("SEARCH:");
        students_frame.add(jLabel8);
        jLabel8.setBounds(710, 60, 60, 40);

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });
        students_frame.add(search);
        search.setBounds(800, 60, 500, 40);

        jLabel16.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        students_frame.add(jLabel16);
        jLabel16.setBounds(0, 0, 1320, 650);

        jPanel1.add(students_frame);
        students_frame.setBounds(20, 50, 1320, 650);

        officer_frame.setBackground(new java.awt.Color(0, 0, 0));
        officer_frame.setLayout(null);

        officer_table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        officer_table.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        officer_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        officer_table.setRowHeight(25);
        officer_table.setSelectionBackground(new java.awt.Color(0, 0, 204));
        officer_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                officer_tableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(officer_table);

        officer_frame.add(jScrollPane2);
        jScrollPane2.setBounds(40, 120, 1250, 440);

        viewoffinfo.setBackground(new java.awt.Color(255, 255, 255));
        viewoffinfo.setText("VIEW OFFICER INFORMATION");
        viewoffinfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewoffinfoActionPerformed(evt);
            }
        });
        officer_frame.add(viewoffinfo);
        viewoffinfo.setBounds(560, 573, 380, 30);

        n4.setBackground(new java.awt.Color(255, 255, 255));
        n4.setText("NEW");
        n4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n4ActionPerformed(evt);
            }
        });
        officer_frame.add(n4);
        n4.setBounds(350, 573, 160, 30);

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("SEARCH:");
        officer_frame.add(jLabel9);
        jLabel9.setBounds(750, 60, 60, 40);

        search1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                search1KeyReleased(evt);
            }
        });
        officer_frame.add(search1);
        search1.setBounds(830, 60, 460, 40);

        jLabel13.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        officer_frame.add(jLabel13);
        jLabel13.setBounds(0, 0, 1320, 650);

        jPanel1.add(officer_frame);
        officer_frame.setBounds(20, 50, 1320, 650);

        penalty_tbl.setBackground(new java.awt.Color(0, 0, 0));
        penalty_tbl.setLayout(null);

        penalty_table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        penalty_table.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        penalty_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        penalty_table.setRowHeight(25);
        penalty_table.setSelectionBackground(new java.awt.Color(0, 0, 204));
        penalty_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                penalty_tableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(penalty_table);

        penalty_tbl.add(jScrollPane5);
        jScrollPane5.setBounds(40, 100, 1270, 470);

        sendsms.setBackground(new java.awt.Color(255, 255, 255));
        sendsms.setText("SEND SMS");
        sendsms.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendsmsActionPerformed(evt);
            }
        });
        penalty_tbl.add(sendsms);
        sendsms.setBounds(380, 580, 170, 30);

        viewpenalty.setBackground(new java.awt.Color(255, 255, 255));
        viewpenalty.setText("VIEW PENALTY INFORMATION");
        viewpenalty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewpenaltyActionPerformed(evt);
            }
        });
        penalty_tbl.add(viewpenalty);
        viewpenalty.setBounds(570, 580, 430, 30);

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("SEARCH:");
        penalty_tbl.add(jLabel11);
        jLabel11.setBounds(650, 50, 60, 40);

        search3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                search3KeyReleased(evt);
            }
        });
        penalty_tbl.add(search3);
        search3.setBounds(720, 50, 580, 40);

        jLabel18.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        penalty_tbl.add(jLabel18);
        jLabel18.setBounds(0, 0, 1320, 650);

        jPanel1.add(penalty_tbl);
        penalty_tbl.setBounds(20, 50, 1320, 650);

        accounts_tbl.setBackground(new java.awt.Color(0, 0, 0));
        accounts_tbl.setLayout(null);

        account_table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        account_table.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        account_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        account_table.setIntercellSpacing(new java.awt.Dimension(0, 0));
        account_table.setRowHeight(25);
        account_table.setSelectionBackground(new java.awt.Color(0, 0, 204));
        account_table.getTableHeader().setReorderingAllowed(false);
        account_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                account_tableMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(account_table);

        accounts_tbl.add(jScrollPane4);
        jScrollPane4.setBounds(40, 110, 1260, 480);

        viewacctim.setBackground(new java.awt.Color(255, 255, 255));
        viewacctim.setText("VIEW ACCOUNT INFORMATION");
        viewacctim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewacctimActionPerformed(evt);
            }
        });
        accounts_tbl.add(viewacctim);
        viewacctim.setBounds(610, 593, 370, 30);

        n1.setBackground(new java.awt.Color(255, 255, 255));
        n1.setText("NEW");
        n1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n1ActionPerformed(evt);
            }
        });
        accounts_tbl.add(n1);
        n1.setBounds(420, 593, 160, 30);

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("SEARCH:");
        accounts_tbl.add(jLabel12);
        jLabel12.setBounds(710, 60, 80, 40);

        search4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                search4KeyReleased(evt);
            }
        });
        accounts_tbl.add(search4);
        search4.setBounds(820, 60, 480, 40);

        jLabel17.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        accounts_tbl.add(jLabel17);
        jLabel17.setBounds(0, 0, 1320, 650);

        jPanel1.add(accounts_tbl);
        accounts_tbl.setBounds(20, 50, 1320, 650);

        record_frame.setBackground(new java.awt.Color(0, 0, 0));
        record_frame.setLayout(null);

        activity_table.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        activity_table.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        activity_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        activity_table.setRowHeight(25);
        activity_table.setSelectionBackground(new java.awt.Color(0, 0, 204));
        activity_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                activity_tableMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(activity_table);

        record_frame.add(jScrollPane6);
        jScrollPane6.setBounds(50, 120, 1250, 470);

        n3.setBackground(new java.awt.Color(255, 255, 255));
        n3.setText("NEW");
        n3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                n3ActionPerformed(evt);
            }
        });
        record_frame.add(n3);
        n3.setBounds(400, 603, 160, 30);

        jButton1.setText("VIEW ACTIVITY INFORMATION");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        record_frame.add(jButton1);
        jButton1.setBounds(600, 603, 330, 30);

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("SEARCH:");
        record_frame.add(jLabel10);
        jLabel10.setBounds(780, 70, 70, 40);

        search2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                search2KeyReleased(evt);
            }
        });
        record_frame.add(search2);
        search2.setBounds(860, 70, 440, 40);

        jLabel20.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        record_frame.add(jLabel20);
        jLabel20.setBounds(0, 0, 1320, 650);

        jPanel1.add(record_frame);
        record_frame.setBounds(20, 50, 1320, 650);

        report_frame.setBackground(new java.awt.Color(0, 0, 0));
        report_frame.setLayout(null);

        printall.setBackground(new java.awt.Color(255, 255, 255));
        printall.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        printall.setText("PRINT ALL RECORDS");
        printall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printallActionPerformed(evt);
            }
        });
        report_frame.add(printall);
        printall.setBounds(500, 230, 424, 50);

        jLabel15.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\M1.png")); // NOI18N
        report_frame.add(jLabel15);
        jLabel15.setBounds(0, 0, 1320, 650);

        jPanel1.add(report_frame);
        report_frame.setBounds(20, 50, 1320, 650);

        account.setBackground(new java.awt.Color(204, 204, 204));
        account.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        account.setText("ACCOUNTS");
        account.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountActionPerformed(evt);
            }
        });
        jPanel1.add(account);
        account.setBounds(830, 700, 150, 40);

        penalty.setBackground(new java.awt.Color(204, 204, 204));
        penalty.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        penalty.setText("GREEN PROJECT");
        penalty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                penaltyActionPerformed(evt);
            }
        });
        jPanel1.add(penalty);
        penalty.setBounds(990, 700, 190, 40);

        reports.setBackground(new java.awt.Color(204, 204, 204));
        reports.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        reports.setText("REPORTS");
        reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsActionPerformed(evt);
            }
        });
        jPanel1.add(reports);
        reports.setBounds(700, 700, 120, 40);

        activities.setBackground(new java.awt.Color(204, 204, 204));
        activities.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        activities.setText("ACTIVITIES");
        activities.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                activitiesActionPerformed(evt);
            }
        });
        jPanel1.add(activities);
        activities.setBounds(225, 700, 140, 40);

        officers.setBackground(new java.awt.Color(204, 204, 204));
        officers.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        officers.setText("OFFICERS");
        officers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                officersActionPerformed(evt);
            }
        });
        jPanel1.add(officers);
        officers.setBounds(520, 700, 170, 40);

        student.setBackground(new java.awt.Color(204, 204, 204));
        student.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        student.setText("STUDENTS");
        student.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentActionPerformed(evt);
            }
        });
        jPanel1.add(student);
        student.setBounds(375, 700, 130, 40);

        jLabel6.setBackground(new java.awt.Color(255, 51, 51));
        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("x");
        jLabel6.setOpaque(true);
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel6);
        jLabel6.setBounds(1320, 0, 50, 34);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 22)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 204, 0));
        jLabel3.setText("NEUST STUDENT ACTIVITIES MONITORING SYSTEM (GREEN PROJECT)");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(360, 0, 820, 50);

        jLabel7.setBackground(new java.awt.Color(0, 51, 153));
        jLabel7.setOpaque(true);
        jPanel1.add(jLabel7);
        jLabel7.setBounds(-6, 700, 1380, 70);

        jLabel5.setBackground(new java.awt.Color(255, 204, 0));
        jLabel5.setOpaque(true);
        jPanel1.add(jLabel5);
        jLabel5.setBounds(0, 670, 1370, 30);

        jLabel19.setBackground(new java.awt.Color(255, 204, 0));
        jLabel19.setOpaque(true);
        jPanel1.add(jLabel19);
        jLabel19.setBounds(0, 50, 1370, 20);

        jLabel4.setBackground(new java.awt.Color(51, 51, 51));
        jLabel4.setOpaque(true);
        jPanel1.add(jLabel4);
        jLabel4.setBounds(0, 50, 1370, 650);

        jLabel1.setBackground(new java.awt.Color(0, 51, 153));
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(1366, 766));
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, -10, 1370, 780);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1366, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(1366, 768));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void studentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentActionPerformed

        students_frame.setVisible(true);
        officer_frame.setVisible(false);
        record_frame.setVisible(false);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(false);
    }//GEN-LAST:event_studentActionPerformed

    private void officersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_officersActionPerformed
        students_frame.setVisible(false);
        officer_frame.setVisible(true);
        record_frame.setVisible(false);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(false);
    }//GEN-LAST:event_officersActionPerformed

    private void activitiesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_activitiesActionPerformed
        students_frame.setVisible(false);
        officer_frame.setVisible(false);
        record_frame.setVisible(true);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(false);

    }//GEN-LAST:event_activitiesActionPerformed

    private void reportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsActionPerformed
        students_frame.setVisible(false);
        officer_frame.setVisible(false);
        record_frame.setVisible(false);
        report_frame.setVisible(true);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(false);
    }//GEN-LAST:event_reportsActionPerformed

    private void penaltyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_penaltyActionPerformed
        students_frame.setVisible(false);
        officer_frame.setVisible(false);
        record_frame.setVisible(false);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(false);
        penalty_tbl.setVisible(true);

        try {
            pst = con.prepareStatement("SELECT * FROM activity_tbl WHERE DATE(date_of_activity) < DATE(NOW())");
            rs = pst.executeQuery();
            while (rs.next()) {
                String settle = rs.getString("activity_number");

                pst = con.prepareStatement("Update penalty_tbl set status='Absent' where status='Unfinished' and activity_number='" + settle + "'");
                pst.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        penalty_tbl();

    }//GEN-LAST:event_penaltyActionPerformed

    private void printallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printallActionPerformed
        try {
            InputStream is = new FileInputStream(new File("C:\\SAMSREPORT\\allrep.jrxml"));
            //InputStream is = new FileInputStream(new File("C:\\Users\\Melissa\\Desktop\\SAMS\\src\\reports\\allrep.jrxml"));
            JasperDesign jd = JRXmlLoader.load(is);
            String sql = "SELECT * from penalty_tbl JOIN students_tbl JOIN activity_tbl where penalty_tbl.id_number = students_tbl.id_number and penalty_tbl.activity_number=activity_tbl.activity_number";
            JRDesignQuery newquery = new JRDesignQuery();
            newquery.setText(sql);
            jd.setQuery(newquery);
            JasperReport jr = JasperCompileManager.compileReport(jd);
            HashMap para = new HashMap();
            JasperPrint j = JasperFillManager.fillReport(jr, para, con);
            JasperViewer.viewReport(j, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_printallActionPerformed

    private void accountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountActionPerformed
        students_frame.setVisible(false);
        officer_frame.setVisible(false);
        record_frame.setVisible(false);
        report_frame.setVisible(false);
        accounts_tbl.setVisible(true);
        penalty_tbl.setVisible(false);
    }//GEN-LAST:event_accountActionPerformed

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Exit/Logout?", "Logout?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
        if (x == 0) {
            new LoginFrame().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jLabel6MouseClicked

    private void n2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n2ActionPerformed
        new StudentFrame().setVisible(true);
        dispose();
        StudentFrame.edit.setVisible(false);
        StudentFrame.delete.setVisible(false);
        StudentFrame.updates.setVisible(false);
    }//GEN-LAST:event_n2ActionPerformed

    private void viewstudinfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewstudinfoActionPerformed
        if (stud_view == 0) {

        } else {
            new StudentFrame().setVisible(true);
            dispose();
            int index = student_table.getSelectedRow();
            StudentFrame.idnum.setText(student_table.getValueAt(index, 0).toString());
            StudentFrame.lname.setText(student_table.getValueAt(index, 1).toString());
            StudentFrame.fname.setText(student_table.getValueAt(index, 2).toString());
            StudentFrame.mname.setText(student_table.getValueAt(index, 3).toString());
            StudentFrame.years.setSelectedItem(student_table.getValueAt(index, 4).toString());
            StudentFrame.sec.setText(student_table.getValueAt(index, 5).toString());
            StudentFrame.prog.setText(student_table.getValueAt(index, 6).toString());
            StudentFrame.add.setText(student_table.getValueAt(index, 7).toString());
            StudentFrame.guardian.setText(student_table.getValueAt(index, 8).toString());
            StudentFrame.guardiancontact.setText(student_table.getValueAt(index, 9).toString());
            StudentFrame.save.setVisible(false);
            StudentFrame.idnum.setEnabled(false);
            StudentFrame.lname.setEnabled(false);
            StudentFrame.fname.setEnabled(false);
            StudentFrame.mname.setEnabled(false);
            StudentFrame.years.setEnabled(false);
            StudentFrame.sec.setEnabled(false);
            StudentFrame.prog.setEnabled(false);
            StudentFrame.cnum.setEnabled(false);
            StudentFrame.guardian.setEnabled(false);
            StudentFrame.guardiancontact.setEnabled(false);
            StudentFrame.add.setEnabled(false);
            stud_view = 0;
            try {
                pst = con.prepareStatement("Select * from students_tbl where id_number='" + student_table.getValueAt(index, 0).toString() + "'");
                rs = pst.executeQuery();
                if (rs.next()) {
                    String cno = rs.getString("contact_number");
                    StudentFrame.cnum.setText(cno);
                }
            } catch (Exception e) {

            }
        }

    }//GEN-LAST:event_viewstudinfoActionPerformed

    private void n4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n4ActionPerformed
        new OfficerFrame().setVisible(true);
        dispose();
        OfficerFrame.q1.setEnabled(true);
        OfficerFrame.jButton6.setVisible(false);
        OfficerFrame.jButton5.setVisible(false);
        OfficerFrame.jButton3.setVisible(false);
    }//GEN-LAST:event_n4ActionPerformed

    private void viewoffinfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewoffinfoActionPerformed
        if (officer_view == 0) {

        } else {
            new OfficerFrame().setVisible(true);
            dispose();
            int index = officer_table.getSelectedRow();
            OfficerFrame.q1.setSelectedItem(officer_table.getValueAt(index, 1).toString());
            OfficerFrame.q2.setText(officer_table.getValueAt(index, 0).toString());
            officer_view = 0;
            OfficerFrame.q1.setEnabled(false);
            OfficerFrame.q2.setEnabled(false);
            OfficerFrame.jButton2.setVisible(false);

        }
    }//GEN-LAST:event_viewoffinfoActionPerformed

    private void officer_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_officer_tableMouseClicked
        officer_view = 1;
    }//GEN-LAST:event_officer_tableMouseClicked

    private void n3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n3ActionPerformed
        new ActivityFrame().setVisible(true);
        dispose();
        ActivityFrame.q1.setEnabled(false);
        ActivityFrame.update.setVisible(false);
        ActivityFrame.edit.setVisible(false);
        ActivityFrame.delete.setVisible(false);
    }//GEN-LAST:event_n3ActionPerformed

    private void viewactinfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewactinfoActionPerformed
//        if (activity_view == 0) {
//
//        } else {
//            new ActivityFrame().setVisible(true);
//            dispose();
//
//            ActivityFrame.q1.setEnabled(false);
//            ActivityFrame.q2.setEnabled(false);
//            ActivityFrame.q3.setEnabled(false);
//            ActivityFrame.q4.setEnabled(false);
//            ActivityFrame.q5.setEnabled(false);
//            ActivityFrame.save.setVisible(false);
//            activity_view = 0;
//            int index = activity_table.getSelectedRow();
//            ActivityFrame.q1.setText(activity_table.getValueAt(index, 0).toString());
//            ActivityFrame.q2.setText(activity_table.getValueAt(index, 1).toString());
//            ActivityFrame.q3.setText(activity_table.getValueAt(index, 2).toString());
//            try {
//                pst = con.prepareStatement("Select * from activity_tbl where activity_number='" + activity_table.getValueAt(index, 0).toString() + "'");
//                rs = pst.executeQuery();
//                if (rs.next()) {
//                    Date date1 = rs.getDate("date_of_activity");
//                    Date date2 = rs.getDate("end_date");
//                    String time1 = rs.getString("Time_start");
//                    String time2 = rs.getString("Time_end");
//                    ActivityFrame.q4.setDate(date1);
//                    ActivityFrame.q5.setDate(date2);
//                    ActivityFrame.tmestart.setText(time1);
//                    ActivityFrame.tmeend.setText(time2);
//                }
//
//            } catch (Exception e) {
//
//            }
//
//        }
//

    }//GEN-LAST:event_viewactinfoActionPerformed

    private void activity_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_activity_tableMouseClicked
        activity_view = 1;
    }//GEN-LAST:event_activity_tableMouseClicked

    private void n1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n1ActionPerformed
        new Accountframe().setVisible(true);
        dispose();
        Accountframe.q1.setEnabled(false);
        Accountframe.jButton2.setVisible(false);
        Accountframe.jButton6.setVisible(false);
        Accountframe.jButton5.setVisible(false);
    }//GEN-LAST:event_n1ActionPerformed

    private void viewacctimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewacctimActionPerformed
        if (account_view == 0) {

        } else {
            new Accountframe().setVisible(true);
            dispose();
            Accountframe.q1.setEnabled(false);
            Accountframe.q2.setEnabled(false);
            Accountframe.q3.setEnabled(false);
            Accountframe.q4.setEnabled(false);
            Accountframe.jButton4.setVisible(false);
            account_view = 0;
            int index = account_table.getSelectedRow();
            Accountframe.q1.setText(account_table.getValueAt(index, 0).toString());
            Accountframe.q2.setText(account_table.getValueAt(index, 1).toString());
//            Accountframe.q4.setSelectedItem(account_table.getValueAt(index, 4).toString());
            try {
                pst = con.prepareStatement("Select * from account_tbl where acc_no='" + account_table.getValueAt(index, 0).toString() + "'");
                rs = pst.executeQuery();
                if (rs.next()) {
                    String usern = rs.getString("user");
//                    String pword = rs.getString("pword");
                    String role = rs.getString("role");
                    Accountframe.q3.setText(usern);
//                    Accountframe.q5.setText(pword);
                    Accountframe.q4.setSelectedItem(role);
//                    Accountframe.Accountframe.role.setSelectedItem(role);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_viewacctimActionPerformed

    private void account_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_account_tableMouseClicked
        account_view = 1;
    }//GEN-LAST:event_account_tableMouseClicked

    private void penalty_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_penalty_tableMouseClicked
        penalty_view = 1;
    }//GEN-LAST:event_penalty_tableMouseClicked

    private void viewpenaltyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewpenaltyActionPerformed
        if (penalty_view == 0) {

        } else {

            new penaltyframe().setVisible(true);
            dispose();

            penaltyframe.q1.setEnabled(false);
            penaltyframe.q2.setEnabled(false);
            penaltyframe.q3.setEnabled(false);
            penaltyframe.q4.setEnabled(false);
            penaltyframe.q5.setEnabled(false);
            penaltyframe.q6.setEnabled(false);
            penaltyframe.q7.setEnabled(false);
            penalty_view = 0;
            int index = penalty_table.getSelectedRow();
            penaltyframe.q1.setText(penalty_table.getValueAt(index, 0).toString());
            penaltyframe.q2.setText(penalty_table.getValueAt(index, 2).toString());
            
            try {
                pst = con.prepareStatement("Select * from activity_tbl where activity_name = '" + penalty_table.getValueAt(index, 1).toString() + "'");
                rs = pst.executeQuery();
                if (rs.next()) {
                    String act_no = rs.getString("activity_number");
                    String descrip = rs.getString("description");
                    penaltyframe.q3.setText(act_no);
                    penaltyframe.q4.setText(descrip);
                }
                pst = con.prepareStatement("Select * from students_tbl where id_number = '" + penalty_table.getValueAt(index, 2).toString() + "'");
                rs = pst.executeQuery();
                if (rs.next()) {
                    String fn = rs.getString("first_name");
                    String mn = rs.getString("middle_name");
                    String ln = rs.getString("last_name");
                    String program = rs.getString("program");
                    penaltyframe.q5.setText(ln + ", " + fn + " " + mn);
                    penaltyframe.q6.setText(program);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }//GEN-LAST:event_viewpenaltyActionPerformed

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        DefaultTableModel table = (DefaultTableModel) student_table.getModel();
        String src = search.getText();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table);
        student_table.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(src));
    }//GEN-LAST:event_searchKeyReleased

    private void search1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search1KeyReleased
        DefaultTableModel table = (DefaultTableModel) officer_table.getModel();
        String src = search1.getText();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table);
        officer_table.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(src));
    }//GEN-LAST:event_search1KeyReleased

    private void search2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search2KeyReleased
        DefaultTableModel table = (DefaultTableModel) activity_table.getModel();
        String src = search2.getText();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table);
        activity_table.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(src));
    }//GEN-LAST:event_search2KeyReleased

    private void search3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search3KeyReleased
        DefaultTableModel table = (DefaultTableModel) penalty_table.getModel();
        String src = search3.getText();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table);
        penalty_table.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(src));
    }//GEN-LAST:event_search3KeyReleased

    private void search4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_search4KeyReleased
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) account_table.getModel();
        String src = search4.getText();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(table);
        account_table.setRowSorter(tr);
        tr.setRowFilter(RowFilter.regexFilter(src));
    }//GEN-LAST:event_search4KeyReleased

    private void sendsmsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendsmsActionPerformed

        try {
            pst = con.prepareStatement("Select * from students_tbl join penalty_tbl join activity_tbl"
                    + " where penalty_tbl.activity_number=activity_tbl.activity_number and penalty_tbl.id_number=students_tbl.id_number   "
                    + "and penalty_tbl.status='Absent'");
            rs = pst.executeQuery();
            while (rs.next()) {
                String name = rs.getString("first_name");
                String cn = rs.getString("contact_number");
                String message = "Good day " + name + ", You have penalty to settle. Please go to registar immediately. Thank you";

                String phone = cn;
                String username = "root";
                String password = "password";
                String address = "http://192.168.1.9";
                String port = "8090";

                try {
                    URL url = new URL(
                            address + ":" + port + "/SendSMS?username=" + username + "&password=" + password
                            + "&phone=" + phone + "&message=" + URLEncoder.encode(message, "ISO-8859-1"));
                    URLConnection connection = url.openConnection();
                    BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String inputline;
                    while ((inputline = bufferedreader.readLine()) != null) {
                        System.out.print(inputline);
                    }
                    bufferedreader.close();
                } catch (MalformedURLException ex) {
                    Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SMS.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_sendsmsActionPerformed

    private void student_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_student_tableMouseClicked

        stud_view = 1;
    }//GEN-LAST:event_student_tableMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (activity_view == 0) {

        } else {
            new ActivityFrame().setVisible(true);
            dispose();

            ActivityFrame.q1.setEnabled(false);
            ActivityFrame.q2.setEnabled(false);
            ActivityFrame.q3.setEnabled(false);
            ActivityFrame.q4.setEnabled(false);
            ActivityFrame.q5.setEnabled(false);
            ActivityFrame.save.setVisible(false);
            activity_view = 0;
            int index = activity_table.getSelectedRow();
            ActivityFrame.q1.setText(activity_table.getValueAt(index, 0).toString());
            ActivityFrame.q2.setText(activity_table.getValueAt(index, 1).toString());
            ActivityFrame.q3.setText(activity_table.getValueAt(index, 2).toString());
            try {
                pst = con.prepareStatement("Select * from activity_tbl where activity_number='" + activity_table.getValueAt(index, 0).toString() + "'");
                rs = pst.executeQuery();
                if (rs.next()) {
                    Date date1 = rs.getDate("date_of_activity");
                    Date date2 = rs.getDate("end_date");
                    String time1 = rs.getString("Time_start");
                    String time2 = rs.getString("Time_end");
                    ActivityFrame.q4.setDate(date1);
                    ActivityFrame.q5.setDate(date2);
                    ActivityFrame.tmestart.setText(time1);
                    ActivityFrame.tmeend.setText(time2);

                }

            } catch (Exception e) {

            }

        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void n5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n5ActionPerformed

        try {
            //StudentFrame.idnum.getText();
            int row1 = student_table.getSelectedRow();
            int row = student_table.convertRowIndexToModel(row1);

            String table_click = student_table.getModel().getValueAt(row, 0).toString();
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new EnumMap<>(EncodeHintType.class);
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            createQRCode(table_click, filePath, charset, hintMap, 250, 250);

            System.out.println("QR Code image created successfully!");
            qrcode q = new qrcode();
            q.setVisible(true);

        } catch (WriterException | IOException ex) {
            Logger.getLogger(MainMenu_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_n5ActionPerformed

    private void n6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_n6ActionPerformed
        new Scanner().setVisible(true);
    }//GEN-LAST:event_n6ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenu_Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenu_Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenu_Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenu_Admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMenu_Admin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton account;
    public static javax.swing.JTable account_table;
    public static javax.swing.JPanel accounts_tbl;
    public static javax.swing.JButton activities;
    public static javax.swing.JTable activity_table;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JButton n1;
    private javax.swing.JButton n2;
    private javax.swing.JButton n3;
    private javax.swing.JButton n4;
    private javax.swing.JButton n5;
    private javax.swing.JButton n6;
    public static javax.swing.JPanel officer_frame;
    public static javax.swing.JTable officer_table;
    public static javax.swing.JButton officers;
    private javax.swing.JButton penalty;
    public static javax.swing.JTable penalty_table;
    public static javax.swing.JPanel penalty_tbl;
    private javax.swing.JButton printall;
    public static javax.swing.JPanel record_frame;
    public static javax.swing.JPanel report_frame;
    private javax.swing.JButton reports;
    private javax.swing.JTextField search;
    private javax.swing.JTextField search1;
    private javax.swing.JTextField search2;
    private javax.swing.JTextField search3;
    private javax.swing.JTextField search4;
    private javax.swing.JButton sendsms;
    private javax.swing.JButton student;
    public static javax.swing.JTable student_table;
    public static javax.swing.JPanel students_frame;
    private javax.swing.JButton viewacctim;
    private javax.swing.JButton viewactinfo;
    private javax.swing.JButton viewoffinfo;
    private javax.swing.JButton viewpenalty;
    private javax.swing.JButton viewstudinfo;
    // End of variables declaration//GEN-END:variables
}
