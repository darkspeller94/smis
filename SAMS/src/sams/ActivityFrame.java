package sams;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import static sams.OfficerFrame.q2;

public class ActivityFrame extends javax.swing.JFrame {

    Connection con = new DbConnection().con();
    ResultSet rs;
    PreparedStatement pst;
    String maxxx;
    String maxxxx;

    public ActivityFrame() {
        initComponents();
        maxCnt();
        maxmid();
    }

    public void maxmid() {
        try {
            pst = con.prepareStatement("Select MAX(penalty_id)MID from penalty_tbl");
            rs = pst.executeQuery();
            if (rs.next()) {
                int max = rs.getInt("mid");
                max++;
                maxxxx = Integer.toString(max);

            } else {
                maxxxx = "1";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void maxCnt() {
        try {
            pst = con.prepareStatement("SELECT MAX(activity_number)MID from activity_tbl");
            rs = pst.executeQuery();
            if (rs.next()) {
                int max = rs.getInt("mid");
                max++;
                maxxx = Integer.toString(max);
                q1.setText(maxxx);
            } else {
                maxxx = "1";
                q1.setText(maxxx);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel8 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        edit = new javax.swing.JButton();
        update = new javax.swing.JButton();
        q1 = new javax.swing.JTextField();
        q2 = new javax.swing.JTextField();
        q3 = new javax.swing.JTextField();
        save = new javax.swing.JButton();
        q5 = new com.toedter.calendar.JDateChooser();
        q4 = new com.toedter.calendar.JDateChooser();
        tmestart = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        tmestart1 = new javax.swing.JTextField();
        tmeend = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        jLabel8.setText("jLabel8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Activity number:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 60, 130, 30);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("Activity name:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(30, 100, 130, 30);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setText("Description:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 140, 130, 30);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setText("Date of activity:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 180, 130, 30);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setText("End of activity:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(30, 220, 130, 30);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Activity Information");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(0, 0, 580, 40);

        back.setText("BACK");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel1.add(back);
        back.setBounds(450, 140, 90, 30);

        delete.setText("DELETE");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        jPanel1.add(delete);
        delete.setBounds(450, 100, 90, 30);

        edit.setText("EDIT");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });
        jPanel1.add(edit);
        edit.setBounds(450, 60, 90, 30);

        update.setText("UPDATE");
        update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateActionPerformed(evt);
            }
        });
        jPanel1.add(update);
        update.setBounds(310, 340, 150, 40);

        q1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                q1ActionPerformed(evt);
            }
        });
        jPanel1.add(q1);
        q1.setBounds(150, 60, 270, 30);
        jPanel1.add(q2);
        q2.setBounds(150, 100, 270, 30);
        jPanel1.add(q3);
        q3.setBounds(150, 140, 270, 30);

        save.setText("SAVE");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });
        jPanel1.add(save);
        save.setBounds(150, 340, 150, 40);

        q5.setDateFormatString("MM-dd-yyyy");
        jPanel1.add(q5);
        q5.setBounds(150, 220, 270, 30);

        q4.setDateFormatString("MM-dd-yyyy");
        jPanel1.add(q4);
        q4.setBounds(150, 180, 270, 30);

        tmestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tmestartActionPerformed(evt);
            }
        });
        tmestart.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tmestartKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tmestartKeyTyped(evt);
            }
        });
        jPanel1.add(tmestart);
        tmestart.setBounds(150, 260, 120, 30);

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel9.setText("Time start:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(30, 260, 130, 30);

        tmestart1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tmestart1ActionPerformed(evt);
            }
        });
        jPanel1.add(tmestart1);
        tmestart1.setBounds(430, 260, 110, 30);

        tmeend.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tmeendKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tmeendKeyTyped(evt);
            }
        });
        jPanel1.add(tmeend);
        tmeend.setBounds(150, 300, 270, 30);

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel10.setText("Time end:");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(30, 300, 130, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\activity1.png")); // NOI18N
        jPanel1.add(jLabel7);
        jLabel7.setBounds(0, 0, 580, 400);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 580, 400);

        setSize(new java.awt.Dimension(578, 399));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
        ActivityFrame.q1.setEnabled(false);
        ActivityFrame.q2.setEnabled(true);
        ActivityFrame.q3.setEnabled(true);
        ActivityFrame.q4.setEnabled(true);
        ActivityFrame.q5.setEnabled(true);
    }//GEN-LAST:event_editActionPerformed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        new MainMenu_Admin().setVisible(true);
        MainMenu_Admin.record_frame.setVisible(true);
        dispose();
    }//GEN-LAST:event_backActionPerformed

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        String stud;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String date1 = dateFormat.format(q4.getDate());
            String date2 = dateFormat.format(q4.getDate());
            pst = con.prepareStatement("INSERT into activity_tbl values (?,?,?,?,?,?,?,?)");
            pst.setString(1, q1.getText());
            pst.setString(2, q2.getText());
            pst.setString(3, q3.getText());
            pst.setString(4, date1);
            pst.setString(5, date2);
            pst.setString(6, tmestart.getText());
            pst.setString(7, tmeend.getText());
            pst.setString(8, tmestart1.getText());

            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to SAVE?", "SAVE?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
            if (x == 0) {

                pst.executeUpdate();

                JOptionPane.showMessageDialog(null, "Added Successfully");
                new MainMenu_Admin().setVisible(true);
                MainMenu_Admin.record_frame.setVisible(true);
                dispose();

            } else {
                JOptionPane.showMessageDialog(null, "Student not Found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement pstt;
            ResultSet rss;
            pstt = con.prepareStatement("Select * from students_tbl");
            rss = pstt.executeQuery();
            while (rss.next()) {

                maxmid();
                stud = rss.getString("id_number");
                System.out.println(stud);
                try {
                    PreparedStatement ps;

                    ps = con.prepareStatement("Insert into penalty_tbl values(?,?,?,?,?)");

                    ps.setString(1, maxxxx);

                    ps.setString(2, q1.getText());
                    ps.setString(3, stud);
                    ps.setString(4, "Unfinished");
                    ps.setString(5, "Unsettled");
                    ps.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_saveActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        try {
            pst = con.prepareStatement("Delete from penalty_tbl where activity_number='" + q1.getText() + "'");
            pst.execute();
            pst = con.prepareStatement("Delete from activity_tbl where activity_number='" + q1.getText() + "'");
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete?", "Delete?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
            if (x == 0) {
                pst.execute();

                JOptionPane.showMessageDialog(null, "Deleted");
                new MainMenu_Admin().setVisible(true);
                dispose();
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_deleteActionPerformed

    private void updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateActionPerformed
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String date1 = dateFormat.format(q4.getDate());
            String date2 = dateFormat.format(q4.getDate());
            pst = con.prepareStatement("Update activity_tbl set activity_name=?,description=?,date_of_activity=?,end_date=?,Time_start=?,Time_end=?,time=? where activity_number='" + q1.getText() + "'");
            pst.setString(1, q2.getText());
            pst.setString(2, q3.getText());
            pst.setString(3, date1);
            pst.setString(4, date2);
            pst.setString(5, tmestart.getText());
            pst.setString(6, tmeend.getText());
            pst.setString(7, tmestart1.getText());
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update?", "Update?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
            if (x == 0) {
                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Updated Successfully");
                new MainMenu_Admin().setVisible(true);
                MainMenu_Admin.record_frame.setVisible(true);
                dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_updateActionPerformed
    public void time_c0mpute() {
        try {
            String hr1, min1, hr2, min2, hrt, mint;
            if (tmestart.getText().length() == 6 && tmeend.getText().length() == 6) {
                hr1 = tmestart.getText().substring(0, 1);
                min1 = tmestart.getText().substring(2, 4);
                hr2 = tmeend.getText().substring(0, 1);
                min2 = tmeend.getText().substring(2, 4);
                if ((tmestart.getText().substring(4, 6).equals("pm") && tmeend.getText().substring(4, 6).equals("pm")
                        || (tmestart.getText().substring(4, 6).equals("am") && tmeend.getText().substring(4, 6).equals("am")))
                        || (tmestart.getText().substring(4, 6).equals("PM") && tmeend.getText().substring(4, 6).equals("PM")
                        || (tmestart.getText().substring(4, 6).equals("AM") && tmeend.getText().substring(4, 6).equals("AM")))) {
                    hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));

                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                } else if (((tmestart.getText().substring(4, 6).equals("am") && tmeend.getText().substring(4, 6).equals("pm"))
                        || (tmestart.getText().substring(4, 6).equals("pm") && tmeend.getText().substring(4, 6).equals("am")))
                        || ((tmestart.getText().substring(4, 6).equals("AM") && tmeend.getText().substring(4, 6).equals("PM"))
                        || (tmestart.getText().substring(4, 6).equals("PM") && tmeend.getText().substring(4, 6).equals("AM")))) {
                    hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                }

            } else if (tmestart.getText().length() == 6 && tmeend.getText().length() == 7) {
                hr1 = tmestart.getText().substring(0, 1);
                min1 = tmestart.getText().substring(2, 4);
                hr2 = tmeend.getText().substring(0, 2);
                min2 = tmeend.getText().substring(3, 5);
                hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
                if ((tmestart.getText().substring(4, 6).equals("pm") && tmeend.getText().substring(5, 7).equals("pm")
                        || (tmestart.getText().substring(4, 6).equals("am") && tmeend.getText().substring(5, 7).equals("am")))
                        || (tmestart.getText().substring(4, 6).equals("PM") && tmeend.getText().substring(5, 7).equals("PM")
                        || (tmestart.getText().substring(4, 6).equals("AM") && tmeend.getText().substring(5, 7).equals("AM")))) {
                    hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));

                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                } else if (((tmestart.getText().substring(4, 6).equals("am") && tmeend.getText().substring(5, 7).equals("pm"))
                        || (tmestart.getText().substring(4, 6).equals("pm") && tmeend.getText().substring(5, 7).equals("am")))
                        || ((tmestart.getText().substring(4, 6).equals("AM") && tmeend.getText().substring(5, 7).equals("PM"))
                        || (tmestart.getText().substring(4, 6).equals("PM") && tmeend.getText().substring(5, 7).equals("AM")))) {
                    hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                }
            } else if (tmestart.getText().length() == 7 && tmeend.getText().length() == 7) {
                hr1 = tmestart.getText().substring(0, 2);
                min1 = tmestart.getText().substring(3, 5);
                hr2 = tmeend.getText().substring(0, 2);
                min2 = tmeend.getText().substring(3, 5);
                hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
                mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
                if ((tmestart.getText().substring(5, 7).equals("pm") && tmeend.getText().substring(5, 7).equals("pm")
                        || (tmestart.getText().substring(5, 7).equals("am") && tmeend.getText().substring(5, 7).equals("am")))
                        || (tmestart.getText().substring(5, 7).equals("PM") && tmeend.getText().substring(5, 7).equals("PM")
                        || (tmestart.getText().substring(5, 7).equals("AM") && tmeend.getText().substring(5, 7).equals("AM")))) {
                    hrt = String.valueOf(Integer.parseInt(hr2) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));

                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                } else if (((tmestart.getText().substring(5, 7).equals("am") && tmeend.getText().substring(5, 7).equals("pm"))
                        || (tmestart.getText().substring(5, 7).equals("pm") && tmeend.getText().substring(5, 7).equals("am")))
                        || ((tmestart.getText().substring(5, 7).equals("AM") && tmeend.getText().substring(5, 7).equals("PM"))
                        || (tmestart.getText().substring(5, 7).equals("PM") && tmeend.getText().substring(5, 7).equals("AM")))) {
                    hrt = String.valueOf((Integer.parseInt(hr2) + 12) - Integer.parseInt(hr1));
                    mint = String.valueOf(Integer.parseInt(min2) - Integer.parseInt(min1));
                    if (Integer.parseInt(mint) < 0) {
                        hrt = String.valueOf(Integer.parseInt(hrt) - 1);
                        mint = String.valueOf(Integer.parseInt(mint) + 60);
                        tmestart1.setText(hrt + ":" + mint);
                    } else {
                        tmestart1.setText(hrt + ":" + mint);
                    }
                }
            }
        } catch (Exception e) {

        }

    }
    private void q1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_q1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_q1ActionPerformed

    private void tmestart1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tmestart1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tmestart1ActionPerformed

    private void tmestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tmestartActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tmestartActionPerformed

    private void tmestartKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tmestartKeyTyped
        // time_c0mpute();
    }//GEN-LAST:event_tmestartKeyTyped

    private void tmeendKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tmeendKeyTyped
//        time_c0mpute();
    }//GEN-LAST:event_tmeendKeyTyped

    private void tmestartKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tmestartKeyReleased
        time_c0mpute();
    }//GEN-LAST:event_tmestartKeyReleased

    private void tmeendKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tmeendKeyReleased
        time_c0mpute();
    }//GEN-LAST:event_tmeendKeyReleased

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ActivityFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ActivityFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ActivityFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ActivityFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ActivityFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    public static javax.swing.JButton delete;
    public static javax.swing.JButton edit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JTextField q1;
    public static javax.swing.JTextField q2;
    public static javax.swing.JTextField q3;
    public static com.toedter.calendar.JDateChooser q4;
    public static com.toedter.calendar.JDateChooser q5;
    public static javax.swing.JButton save;
    public static javax.swing.JTextField tmeend;
    public static javax.swing.JTextField tmestart;
    public static javax.swing.JTextField tmestart1;
    public static javax.swing.JButton update;
    // End of variables declaration//GEN-END:variables
}
