package sams;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

public class Accountframe extends javax.swing.JFrame {

    Connection con = new DbConnection().con();
    ResultSet rs;
    PreparedStatement pst;
    String maxxx;

    public Accountframe() {
        initComponents();
        maxCnt();
    }

    void maxCnt() {
        try {
            pst = con.prepareStatement("SELECT MAX(acc_no)MID from account_tbl");
            rs = pst.executeQuery();
            if (rs.next()) {
                int max = rs.getInt("mid");
                max++;
                maxxx = Integer.toString(max);
                q1.setText(maxxx);
            } else {
                maxxx = "1";
                q1.setText(maxxx);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        q1 = new javax.swing.JTextField();
        q2 = new javax.swing.JTextField();
        q3 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        q4 = new javax.swing.JComboBox<>();
        q5 = new javax.swing.JPasswordField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        jLabel7.setText("jLabel7");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Account Number:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 110, 130, 30);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("ID Number:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 150, 130, 30);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setText("Password:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 230, 130, 30);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setText("Role:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(40, 270, 130, 30);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Account Information");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(0, 0, 580, 50);

        jButton1.setText("BACK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(460, 190, 90, 30);

        jButton5.setText("DELETE");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton5);
        jButton5.setBounds(460, 150, 90, 30);

        jButton6.setText("EDIT");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6);
        jButton6.setBounds(460, 110, 90, 30);

        jButton2.setText("UPDATE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(300, 330, 150, 40);
        getContentPane().add(q1);
        q1.setBounds(160, 110, 270, 30);
        getContentPane().add(q2);
        q2.setBounds(160, 150, 270, 30);
        getContentPane().add(q3);
        q3.setBounds(160, 190, 270, 30);

        jButton4.setText("SAVE");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(120, 330, 150, 40);

        q4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ADMIN", "OFFICER", "STUDENT" }));
        q4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                q4ActionPerformed(evt);
            }
        });
        getContentPane().add(q4);
        q4.setBounds(160, 270, 270, 30);
        getContentPane().add(q5);
        q5.setBounds(160, 230, 270, 30);

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setText("Username:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(30, 190, 130, 30);

        jLabel9.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\SAMS\\src\\sams\\pictures\\neust logo1.png")); // NOI18N
        jLabel9.setText("jLabel9");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(-20, 0, 220, 110);

        jLabel4.setIcon(new javax.swing.ImageIcon("C:\\Users\\Melissa\\Desktop\\activity1.png")); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 580, 400);

        setSize(new java.awt.Dimension(576, 394));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new MainMenu_Admin().setVisible(true);
        MainMenu_Admin.accounts_tbl.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try {
            pst = con.prepareStatement("Delete from account_tbl where acc_no='" + q1.getText() + "'");
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Delete?", "Delete?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
            if (x == 0) {
                pst.execute();
                pst = con.prepareStatement("Delete from accounts_tbl where acc_no='" + q1.getText() + "'");
                pst.execute();
                JOptionPane.showMessageDialog(null, "Deleted");
                new MainMenu_Admin().setVisible(true);
                dispose();
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        q1.setEnabled(false);
        q2.setEnabled(true);
        q3.setEnabled(true);
        q4.setEnabled(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {

            pst = con.prepareStatement("Update account_tbl set user=?,password=?,role=? where acc_no='" + q1.getText() + "'");

            pst.setString(1, q3.getText());
            pst.setString(2, q5.getText());
            pst.setString(3, q4.getSelectedItem().toString());
            int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to Update?", "Update?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
            if (x == 0) {
                pst.executeUpdate();
                pst = con.prepareStatement("Update accounts_tbl set id_number=? where acc_no='" + q1.getText() + "'");
                pst.setString(1, q2.getText());
                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Updated Successfully");
                new MainMenu_Admin().setVisible(true);
                MainMenu_Admin.accounts_tbl.setVisible(true);
                dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            pst = con.prepareStatement("Select * from account_tbl where user = '" + q3.getText() + "'");
            rs = pst.executeQuery();
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "USERNAME Already Exist");
            } else {
                try {
                    pst = con.prepareStatement("INSERT into account_tbl values (?,?,?,?)");
                    pst.setString(1, q1.getText());

                    pst.setString(2, q3.getText());
                    pst.setString(3, q5.getText());
                    pst.setString(4, q4.getSelectedItem().toString());
                    int x = JOptionPane.showConfirmDialog(null, "Are you sure you want to SAVE?", "SAVE?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
                    if (x == 0) {

                        pst.executeUpdate();
                        pst = con.prepareStatement("INSERT into accounts_tbl values (?,?)");
                        pst.setString(1, q1.getText());
                        pst.setString(2, q2.getText());
                        pst.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Added Successfully");
                        new MainMenu_Admin().setVisible(true);
                        MainMenu_Admin.accounts_tbl.setVisible(true);
                        dispose();

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void q4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_q4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_q4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Accountframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Accountframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Accountframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Accountframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Accountframe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    public static javax.swing.JButton jButton2;
    public static javax.swing.JButton jButton4;
    public static javax.swing.JButton jButton5;
    public static javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public static javax.swing.JTextField q1;
    public static javax.swing.JTextField q2;
    public static javax.swing.JTextField q3;
    public static javax.swing.JComboBox<String> q4;
    public static javax.swing.JPasswordField q5;
    // End of variables declaration//GEN-END:variables
}
